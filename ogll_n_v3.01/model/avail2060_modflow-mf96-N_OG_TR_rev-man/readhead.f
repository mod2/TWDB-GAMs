c Program to read UNFORMATTED head or drawdown file written by MODFLOW
c
      IMPLICIT NONE
      INTEGER :: NUM
      PARAMETER (NUM=1000)
      INTEGER :: I, J, K, KARRAYS, N, IOIN1, IST,
     >            NCOL, NCOLDIS, NLAY, NROW, NROWDIS, INORIG,
     >            IOUT, ITS, ISP, KAR, iarg
      INTEGER, DIMENSION(50) :: IU
      INTEGER, DIMENSION(NUM) :: ILAY, IPER, ISTP, KSTP, KPER
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: VAL
      REAL, DIMENSION(NUM) :: PERTIM, TOTIM
      REAL :: BLANKING
      REAL :: HNOFLO, HNOFLOM, HNOFLOP, HDRY, HDRYM, HDRYP, VAL_NODATA
      CHARACTER(LEN=16) :: TEXT(NUM)
      CHARACTER(LEN=2000) :: INFIL, HEDFIL
      CHARACTER*24 :: OUTFIL
      CHARACTER(LEN=80) :: comlin
c
c   Format statements
c
  600 FORMAT(1X,'Error opening file: ',A)
  640 FORMAT(1X,' HNOFLO = ',G14.7,' and HDRY = ',G14.7,/)
  690 FORMAT(1X,'      Row    Column     Layer Str. Per.',
     >  '  Timestep                Head')
  700 FORMAT(5I10,ES20.8)
c
c --- Read year from command line
c
      comlin = ' '
      iarg = 1
      CALL GETARG(iarg,comlin)
      READ(comlin,*) ISP
c
c Initialize scalars
c
      BLANKING=0.1701412E+39  ! Surfer "blanking" value
      VAL_NODATA = -9999.9
      IOIN1=7
      INORIG=13
      IOUT=14
      NLAY=1
      NROWDIS=188
      NCOLDIS=256
      ITS=12
      HNOFLO=-999.990
      HDRY=-1.000e+03
      HEDFIL='N_Ogallala-Tran.hds'
      OUTFIL='N_Ogallala-Tran_HEAD.txt'
c Initialize arrays
      IU=0
c
c See if a file named modeldata.txt exists.  IF so, read model data
c
      WRITE(*,640)HNOFLO,HDRY
      IF (HNOFLO>0.0) THEN
        HNOFLOM=0.999*HNOFLO
        HNOFLOP=1.001*HNOFLO
      ELSEIF (HNOFLO<0.0) THEN
        HNOFLOM=1.001*HNOFLO
        HNOFLOP=0.999*HNOFLO
      ELSE
        HNOFLOM=-1.E-30
        HNOFLOP=1.E30
      ENDIF
      IF (HDRY>0.0) THEN
        HDRYM=0.999*HDRY
        HDRYP=1.001*HDRY
      ELSEIF (HNOFLO<0.0) THEN
        HDRYM=1.001*HDRY
        HDRYP=0.999*HDRY
      ELSE
        HDRYM=-1.E-30
        HDRYP=1.E30
      ENDIF
c
      OPEN(IOUT,FILE=OUTFIL,STATUS='REPLACE')
c
      ALLOCATE (VAL(NCOLDIS,NROWDIS,1))
c
      INFIL=HEDFIL
c
c  Open unformatted head or drawdown file
      OPEN(IOIN1,FILE=INFIL,ACCESS='SEQUENTIAL',FORM='BINARY',
     >  STATUS='OLD',IOSTAT=IST)
      IF (IST .NE. 0) THEN
        WRITE(*,600)TRIM(INFIL)
        GOTO 9000
      ENDIF
c
c Count arrays in binary file
      KARRAYS = 0
      DO WHILE (.TRUE.)
c Read an identifying record and array of data for one layer for
c one time step
        READ(IOIN1,ERR=75,END=76)KSTP(1),KPER(1),PERTIM(1),TOTIM(1),
     >    TEXT(1),NCOL,NROW,ILAY(1)
        IF (NCOL.NE.NCOLDIS) THEN
          WRITE(*,72)NCOL,NCOLDIS
          STOP
        ENDIF
   72   FORMAT('Error reading binary file: NCOL read from binary',
     >    ' file (',I15, ') does not match NCOL read from ',
     >    'discretization file (',I8,')')
        IF (NROW.NE.NROWDIS) THEN
          WRITE(*,73)NROWDIS,NROW
          STOP
        ENDIF
   73   FORMAT('Error reading binary file: NROW read from binary',
     >    ' file (',I15,') does not match NROW read from',
     >    ' modeldata.txt file (',I8,')')
c Read one layer of values
        do i=1,nrow
          READ(IOIN1,ERR=75,END=76)(VAL(J,I,1),J=1,NCOL)
        enddo
        KARRAYS=KARRAYS+1
      ENDDO
   75 CONTINUE
      WRITE(*,*)'Error in read statement'
      STOP
   76 CONTINUE
      DEALLOCATE (VAL)
      ALLOCATE (VAL(NCOL,NROW,KARRAYS))
c
      REWIND(IOIN1)
c
      DO 80 N=1,KARRAYS
c
c Read an identifying record and array of data for one layer for
c one time step
c
c Read identifying record
        READ(IOIN1,ERR=105,END=110)KSTP(N),KPER(N),PERTIM(N),TOTIM(N),
     >    TEXT(N),NCOL,NROW,ILAY(N)
c Read one layer of values
        READ(IOIN1,ERR=105,END=110)((VAL(J,I,N),J=1,NCOL),I=1,NROW)
   80 CONTINUE
      GOTO 110
c
  105 CONTINUE
      WRITE(*,*)'Error in read statement'
      STOP
c
  110 CONTINUE
  120 CONTINUE
c
c   Write out selected Stress Period/Time Step
c
      WRITE(IOUT,690)
      DO KAR=1,KARRAYS
        IF (KSTP(KAR).EQ.ITS .AND. KPER(KAR).EQ.ISP) THEN
          DO J=1,NROW
            DO I=1,NCOL
              WRITE(IOUT,700) J, I, ILAY(KAR), ISP, ITS, VAL(I,J,KAR)
            ENDDO
          ENDDO
          GOTO 9000
        ENDIF
      ENDDO
c
 9000 CONTINUE
      STOP
      END
