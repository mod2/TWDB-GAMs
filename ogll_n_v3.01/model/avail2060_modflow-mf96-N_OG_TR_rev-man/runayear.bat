pump %1
DEL N_Ogallala-Tran.hds
MODFLOW.EXE < namefile2
move /Y N_OG.out N_OG_%1.out
move /Y N_Ogallala-Tran.ddn N_Ogallala-Tran_%1.ddn
move /Y N_Ogallala-Tran.cbb N_Ogallala-Tran_%1.cbb
move /Y N_Ogallala-Tran.hds N_Ogallala-Tran_%1.hds
move /Y N_Ogallala-Tran_mod.wel N_Ogallala-Tran_mod_%1.wel
move /Y N_Ogallala-Tran_prev.hds N_Ogallala-Tran_prev.hds_%1.hds
copy N_Ogallala-Tran_%1.hds N_Ogallala-Tran_prev.hds
UNFtoBIN N_Ogallala-Tran_%1.hds N_Ogallala-Tran.hds
readhead 1
gwinplac
move /Y N_Ogallala-Tran_GWINPLAC.txt N_Ogallala-Tran_GWINPLAC_%1.txt
