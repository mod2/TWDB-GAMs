c       ************************ PROGRAM GWINPLAC.F ****************************
c          Determines groundwater in each cell.
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
c
      DIMENSION head(188,256),bott(188,256),specyld(188,256)
      DIMENSION gwincell(188,256),satthk(188,256)
      INTEGER subdiv(188,256)
c
      head=-1000000.0
      bott=-1000000.0
      gwincell=-1000000.0
      subdiv=-1000000.0
      specyld=-1000000.0
c
c --- Read specific yield.
c
      insy=9
      OPEN(insy,FILE='N_Ogallala-Tran_STORAGE.txt',STATUS='OLD'
     >  ,IOSTAT=IST)
   80 READ(insy,*,END=90) ir,ic,il,value
      specyld(ir,ic)=value
      GOTO 80
   90 CLOSE(insy)
      WRITE(*,*) 'Read specific yield'
c
c --- Read head.
c
      inhed=11
      OPEN(inhed,FILE='N_Ogallala-Tran_HEAD.txt',STATUS='OLD'
     >  ,IOSTAT=IST)
      READ(inhed,*)
  120 READ(inhed,"(5I10,ES20.8)",END=130) ir,ic,il,isp, its, value
      head(ir,ic)=value
      GOTO 120
  130 CLOSE(inhed)
      WRITE(*,*) 'Read head'
c
c --- Read base of layer 1.
c
      inbot=12
      OPEN(inbot,FILE='N_Ogallala-Tran_BOTTOM.txt',STATUS='OLD'
     >  ,IOSTAT=IST)
  140 READ(inbot,*,END=150) ir,ic,il,value
      bott(ir,ic)=value
      satthk(ir,ic)=head(ir,ic)-bott(ir,ic)
      GOTO 140
  150 CLOSE(inbot)
      WRITE(*,*) 'Read bottom'
c
c --- Read subdivision number.
c
      insub=13
      OPEN(insub,FILE='Subdivision.csv',STATUS='OLD'
     >  ,IOSTAT=IST)
      READ(insub,*)
  160 READ(insub,*,END=170) ir,ic,idum,ivalue
      subdiv(ir,ic)=ivalue
      GOTO 160
  170 CLOSE(insub)
      WRITE(*,*) 'Read subdivision'
c
c --- Calculate volume of water contained in each cell.
c
      OPEN(22,FILE='N_Ogallala-Tran_GWINPLAC.txt',
     >  STATUS='REPLACE',IOSTAT=IST)
      hedsums1=0.0
      hedsums2=0.0
      hedsums3=0.0
      DO ir=1,188
        DO ic=1,256
          IF((subdiv(ir,ic).gt.0).and.
     >      (head(ir,ic).gt.0.0).and.(head(ir,ic).lt.10000.0)) THEN
            gwincell(ir,ic)=satthk(ir,ic)*5280*5280*specyld(ir,ic)
            IF(subdiv(ir,ic).eq.1) hedsums1=hedsums1+
     >        gwincell(ir,ic)
            IF(subdiv(ir,ic).eq.2) hedsums2=hedsums2+
     >        gwincell(ir,ic)
            IF(subdiv(ir,ic).eq.3) hedsums3=hedsums3+
     >        gwincell(ir,ic)
          ENDIF
          WRITE(22,"(2I10,ES20.8)") ir,ic,gwincell(ir,ic)
        ENDDO
      ENDDO
      WRITE(22,"(a15,F20.3)") 'Subdivision 1  ',hedsums1/43560.0
      WRITE(22,"(a15,F20.3)") 'Subdivision 2  ',hedsums2/43560.0
      WRITE(22,"(a15,F20.3)") 'Subdivision 3  ',hedsums3/43560.0
c
      GOTO 9999
c
 9999 CONTINUE
      STOP
      END


