The Steady State model files are located in the ss_modflow folder.

To run the model execute "modflow.exe < namefile" in a shell.

For questions contact gam@twdb.texas.gov.
