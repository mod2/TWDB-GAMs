The Availability model files are located in the avail2060_modflow folder.

To run the model execute "run_it.bat" in a shell.

There are 111 stress periods (1950 - 2060).
The predictive availability simulation covers 2010-2060.
The simulation was designed to satisfy the 2009 Desired Future Conditions.

For questions contact gam@twdb.texas.gov.
