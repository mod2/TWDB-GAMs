The Transient model files are located in the tr_modflow folder.

To run the model execute "modflow.exe < namefile" in a shell.

There are 111 stress periods (1950 - 2060).
Last stress period of calibration is 59 (2008).

For questions contact gam@twdb.texas.gov.
