MODFLOW.EXE < namefile1
move /Y N_Ogallala-Tran.out N_Ogallala-Tran_2009.out
move /Y N_Ogallala-Tran.ddn N_Ogallala-Tran_2009.ddn
move /Y N_Ogallala-Tran.cbb N_Ogallala-Tran_2009.cbb
move /Y N_Ogallala-Tran.hds N_Ogallala-Tran_2009.hds
copy N_Ogallala-Tran_2009.hds N_Ogallala-Tran_prev.hds
UNFtoBIN N_Ogallala-Tran_2009.hds N_Ogallala-Tran.hds
readhead 55
gwinplac
move /Y N_Ogallala-Tran_GWINPLAC.txt N_Ogallala-Tran_GWINPLAC_2009.txt
FOR %%a IN (2010 2011 2012 2013 2014 2015 2016 2017 2018 2019)  DO CALL RUNAYEAR %%a
FOR %%a IN (2020 2021 2022 2023 2024 2025 2026 2027 2028 2029)  DO CALL RUNAYEAR %%a
FOR %%a IN (2030 2031 2032 2033 2034 2035 2036 2037 2038 2039)  DO CALL RUNAYEAR %%a
FOR %%a IN (2040 2041 2042 2043 2044 2045 2046 2047 2048 2049)  DO CALL RUNAYEAR %%a
FOR %%a IN (2050 2051 2052 2053 2054 2055 2056 2057 2058 2059 2060)  DO CALL RUNAYEAR %%a
pause

