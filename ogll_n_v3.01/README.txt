The update to the 2004 GAM includes the following:
1. Revise and update pumping in the GAM to include historical estimates through the year 2008 and to include future demand estimates through the year 2060;
2. Incorporate additional data available on aquifer properties including hydraulic conductivity, bedrock morphology (base of Ogallala aquifer or top of red beds), and specific yield;
3. Estimate aquifer conditions under projected groundwater demand and perform simulations to support the estimation of groundwater availability within the Northern Ogallala in Texas.

See the report included with the model files for a complete picture of chages.

For questions about the model contact the TWDB's GAM program at gam@twdb.texas.gov.